<?php

/**
 * @file
 * Audit log classes.
 */

class AuditLogMessage {

  public $entity_type = NULL;
  public $bundle = NULL;
  public $entity_id = NULL;
  public $revision_id = NULL;
  public $action = '';
  public $timestamp = REQUEST_TIME;
  public $uid = 0;
  public $messages = array();


  public function __construct() {
    global $user;
    $this->uid = $user->uid;
  }

  /**
   * Write the log message to the database.
   */
  public function save() {
    // Create the message string.
    if (count($this->messages) > 1) {
      $message = theme('item_list', array('items' => $this->messages));
    }
    else {
      $message = $this->messages[0];
    }

    $record = array(
      'entity_type' => $this->entity_type,
      'bundle' => $this->bundle,
      'entity_id' => $this->entity_id,
      'revision_id' => $this->revision_id,
      'action' => $this->action,
      'timestamp' => $this->timestamp,
      'uid' => $this->uid,
      'message' => $message,
    );
    drupal_write_record('audit_log', $record);
  }

  /**
   * Map actions to friendly strings.
   */
  public function getActionFriendlyName($action) {
    switch ($action) {
      case 'insert':
        return t('created');
      case 'update':
        return t('updated');
      case 'delete':
        return t('deleted');
      default:
        return $action;
    }
  }

}

/**
 * Handle users.
 */
class AuditLogMessageUser extends AuditLogMessage {
  public function __construct($account, $action) {
    parent::__construct();
    $this->entity_type = 'user';
    $this->bundle = 'user';
    $this->entity_id = $account->uid;
    $this->action = $action;

    // Create the message string.
    $acting_account = user_load($this->uid);
    $args = array(
      '@action' => $this->getActionFriendlyName($action),
      '!user' => l($account->name, 'user/' . $account->uid),
    );
    if ($account->uid) {
      $args['!acting_user'] = l($acting_account->name, 'user/' . $acting_account->uid);
    }
    else {
      $args['!acting_user'] = check_plain($acting_account->name);
    }
    $this->messages[] = t('!acting_user @action user !user', $args);

    // Allow modules to inject additional data.
    module_load_include('inc', 'audit_log', 'modules/user');
    $module_messages = module_invoke_all('audit_log_user_messages', $account, $action);
    foreach ($module_messages as $message) {
      $this->messages[] = $message;
    }
  }
}

/**
 * Handle nodes.
 */
class AuditLogMessageNode extends AuditLogMessage {
  public function __construct($node, $action) {
    parent::__construct();
    $this->entity_type = 'node';
    $this->bundle = $node->type;
    $this->entity_id = $node->nid;
    $this->revision_id = $node->vid;
    $this->action = $action;

    // Create the message string.
    $account = user_load($this->uid);
    $args = array(
      '@action' => $this->getActionFriendlyName($action),
      '!node' => l($node->title, 'node/' . $node->nid),
    );
    if ($account->uid) {
      $args['!user'] = l($account->name, 'user/' . $account->uid);
    }
    else {
      $args['!user'] = check_plain($account->name);
    }
    $this->messages[] = t('!user @action node !node', $args);

    // Allow modules to inject additional data.
    module_load_include('inc', 'audit_log', 'modules/node');
    if (module_exists('forum')) {
      module_load_include('inc', 'audit_log', 'modules/forum');
    }
    if (module_exists('diff')) {
      module_load_include('inc', 'audit_log', 'modules/diff');
    }
    $module_messages = module_invoke_all('audit_log_node_messages', $node, $action);
    foreach ($module_messages as $message) {
      $this->messages[] = $message;
    }
  }
}

/**
 * Handle comments.
 */
class AuditLogMessageComment extends AuditLogMessage {
  public function __construct($comment, $action) {
    parent::__construct();
    $this->entity_type = 'comment';
    $this->bundle = $comment->node_type;
    $this->entity_id = $comment->cid;
    $this->action = $action;

    // Create the message string.
    $account = user_load($this->uid);
    $uri = comment_uri($comment);
    $args = array(
      '@action' => $this->getActionFriendlyName($action),
      '!comment' => l('#' . $comment->cid, $uri['path'], array('options' => $uri['options'])),
      '!node' => l('#' . $comment->nid, 'node/' . $comment->nid),
    );
    if ($account->uid) {
      $args['!user'] = l($account->name, 'user/' . $account->uid);
    }
    else {
      $args['!user'] = check_plain($account->name);
    }
    $this->messages[] = t('!user @action comment !comment on node !node', $args);

    // Allow modules to inject additional data.
    module_load_include('inc', 'audit_log', 'modules/comment');
    $module_messages = module_invoke_all('audit_log_comment_messages', $comment, $action);
    foreach ($module_messages as $message) {
      $this->messages[] = $message;
    }
  }
}

/**
 * Handle other entities.
 */
class AuditLogMessageEntity extends AuditLogMessage {
  public function __construct($entity, $entity_type, $action) {
    parent::__construct();
    list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
    $this->entity_type = $entity_type;
    $this->bundle = $bundle;
    $this->entity_id = $id;
    $this->action = $action;

    // Create the message string.
    $account = user_load($this->uid);
    $uri = entity_uri($entity_type, $entity);
    $args = array(
      '@action' => $this->getActionFriendlyName($action),
      '!entity' => l('#' . $id, $uri['path'], array('options' => $uri['options'])),
      '@type' => $entity_type,
    );
    if ($account->uid) {
      $args['!user'] = l($account->name, 'user/' . $account->uid);
    }
    else {
      $args['!user'] = check_plain($account->name);
    }
    $this->messages[] = t('!user @action entity @type !entity', $args);

    // Allow modules to inject additional data.
    $module_messages = module_invoke_all('audit_log_entity_messages', $entity, $entity_type, $action);
    foreach ($module_messages as $message) {
      $this->messages[] = $message;
    }
  }
}

/**
 * Handle IP address blocking.
 */
class AuditLogMessageIPBlocking extends AuditLogMessage {
  public function __construct($ip, $action) {
    parent::__construct();
    $this->entity_type = 'ip_address';
    $this->bundle = check_plain($ip);
    $this->action = $action;

    // Create the message string.
    $account = user_load($this->uid);
    $args = array(
      '@action' => $this->getActionFriendlyName($action),
      '@ip' => $ip,
      '!user' => l($account->name, 'user/' . $account->uid),
    );
    $this->messages[] = t('!user @action blocked IP @ip', $args);

    // Allow modules to inject additional data.
    $module_messages = module_invoke_all('audit_log_ip_blocking_messages', $ip, $action);
    foreach ($module_messages as $message) {
      $this->messages[] = $message;
    }
  }
}