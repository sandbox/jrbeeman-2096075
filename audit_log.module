<?php

/**
 * @file
 * Audit log module.
 *
 * @todo Add menu items.
 * @todo Add taxonomy terms.
 */

/**
 * Implements hook_permission().
 */
function audit_log_permission() {
  return array(
    'access audit log' => array(
      'title' => t('Access audit log reports'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * --------------------------------------------------------
 * Users
 * --------------------------------------------------------
 */

/**
 * Implements hook_user_insert().
 */
function audit_log_user_insert(&$edit, $account, $category) {
  // Temporarily set the user object so that we get the right logging info.
  global $user;
  $tmp_user = NULL;
  if (user_is_anonymous()) {
    $tmp_user = $user;
    $user = $account;
  }

  // Save the message.
  $message = new AuditLogMessageUser($account, 'insert');
  $message->save();

  // Reset the user object.
  if ($tmp_user) {
    $user = $tmp_user;
  }
}

/**
 * Implements hook_user_update().
 */
function audit_log_user_update(&$edit, $account, $category) {
  // Temporarily set the user object so that we get the right logging info.
  global $user;
  $tmp_user = NULL;
  if (user_is_anonymous()) {
    $tmp_user = $user;
    $user = $account;
  }

  // Save the message.
  $message = new AuditLogMessageUser($account, 'update');
  $message->save();

  // Reset the user object.
  if ($tmp_user) {
    $user = $tmp_user;
  }
}

/**
 * Implements hook_user_delete().
 */
function audit_log_user_delete($account) {
  $message = new AuditLogMessageUser($account, 'delete');
  $message->save();
}

/**
 * --------------------------------------------------------
 * Nodes
 * --------------------------------------------------------
 */

/**
 * Implements hook_node_insert().
 */
function audit_log_node_insert($node) {
  if (empty($node->type) || empty($node->nid) || empty($node->nid)) {
    return;
  }
  $message = new AuditLogMessageNode($node, 'insert');
  $message->save();
}

/**
 * Implements hook_node_update().
 */
function audit_log_node_update($node) {
  if (empty($node->type) || empty($node->nid) || empty($node->nid)) {
    return;
  }
  $message = new AuditLogMessageNode($node, 'update');
  $message->save();
}

/**
 * Implements hook_node_delete().
 */
function audit_log_node_delete($node) {
  if (empty($node->type) || empty($node->nid) || empty($node->nid)) {
    return;
  }
  $message = new AuditLogMessageNode($node, 'delete');
  $message->save();
}

/**
 * --------------------------------------------------------
 * Comments
 * --------------------------------------------------------
 */

/**
 * Implements hook_comment_insert().
 */
function audit_log_comment_insert($comment) {
  $message = new AuditLogMessageComment($comment, 'insert');
  $message->save();
}

/**
 * Implements hook_comment_update().
 */
function audit_log_comment_update($comment) {
  $message = new AuditLogMessageComment($comment, 'update');
  $message->save();
}

/**
 * Implements hook_comment_delete().
 */
function audit_log_comment_delete($comment) {
  $message = new AuditLogMessageComment($comment, 'delete');
  $message->save();
}

/**
 * --------------------------------------------------------
 * Other entities
 * --------------------------------------------------------
 */

/**
 * Implements hook_entity_insert().
 */
function audit_log_entity_insert($entity, $type) {
  if (!audit_log_is_logged_entity_type($type)) {
    return;
  }
  $message = new AuditLogMessageEntity($entity, $type, 'insert');
  $message->save();
}

/**
 * Implements hook_entity_update().
 */
function audit_log_entity_update($entity, $type) {
  if (!audit_log_is_logged_entity_type($type)) {
    return;
  }
  $message = new AuditLogMessageEntity($entity, $type, 'update');
  $message->save();
}

/**
 * Implements hook_entity_delete().
 */
function audit_log_entity_delete($entity, $type) {
  if (!audit_log_is_logged_entity_type($type)) {
    return;
  }
  $message = new AuditLogMessageEntity($entity, $type, 'delete');
  $message->save();
}

/**
 * --------------------------------------------------------
 * Form submissions (non-entities).
 * --------------------------------------------------------
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function audit_log_form_system_ip_blocking_form_alter(&$form, &$form_state) {
  $form['#submit'][] = 'audit_log_ip_blocking_insert';
}

function audit_log_ip_blocking_insert($form, &$form_state) {
  $ip = $form_state['values']['ip'];
  $message = new AuditLogMessageIPBlocking($ip, 'insert');
  $message->save();
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function audit_log_form_system_ip_blocking_delete_alter(&$form, &$form_state) {
  $submit = $form['#submit'];
  $form['#submit'] = array('audit_log_ip_blocking_delete');
  foreach ($submit as $callback) {
    $form['#submit'][] = $callback;
  }
}

function audit_log_ip_blocking_delete($form, &$form_state) {
  $ip = $form_state['values']['blocked_ip']['ip'];
  $message = new AuditLogMessageIPBlocking($ip, 'delete');
  $message->save();
}

/**
 * --------------------------------------------------------
 * Miscellaneous
 * --------------------------------------------------------
 */

/**
 * Implements hook_views_api().
 */
function audit_log_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Get list of entity types that are ignored in hook_entity_*().
 *
 * @todo Figure out which entities to log here.
 */
function audit_log_is_logged_entity_type($type) {
  $logged = array();
  if (!empty($logged[$type])) {
    return TRUE;
  }
}
