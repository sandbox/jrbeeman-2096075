<?php

/**
 * @file
 * Expose audit log module data to Views.
 */

/**
 * Implements hook_views_data().
 */
function audit_log_views_data() {
  $data = array();

  $data['audit_log']['table']['group'] = t('Audit log');
  $data['audit_log']['table']['base'] = array(
    'field' => 'mid',
    'title' => t('Audit log entries'),
    'help' => t('Contains logged messages for auditing changes to entities.'),
  );

  // Create relationships to referenced entities.
  foreach (entity_get_info() as $entity_type => $entity_info) {
    $table = $entity_info['base table'];
    $entity_key = $entity_info['entity keys']['id'];
    // Create the join to entity base tables.
    $data['audit_log']['table']['join'][$table] = array(
      'left_field' => $entity_key,
      'field' => 'entity_id',
    );
    // Create the relationship link to the entity. Use field name alias.
    $field_name =  $table . '_audit_log_target_id';
    $parameters = array(
      '@entity' => $table,
      '@field_name' => 'audit log entity reference',
    );
    $data['audit_log'][$field_name]['title'] = t($table . ' logged');
    $data['audit_log'][$field_name]['help'] = t('ID of the audited entity.');
    $data['audit_log'][$field_name]['relationship'] = array(
      'handler' => 'views_handler_relationship',
      'base' => $table,
      'base field' => $entity_key,
      'field' => 'entity_id',
      'label' => t('@entity entity referenced from @field_name', $parameters),
      'group' => t('Entity reference'),
      'title' => t('Referenced entity'),
      'help' => t('A bridge to the @entity entity that is referenced via @field_name', $parameters),
    );
  }

  // Declare our fields.
  $data['audit_log']['entity_type'] = array(
    'title' => t('Entity type'),
    'help' => t('The type of the audited entity.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => FALSE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['audit_log']['bundle'] = array(
    'title' => t('Entity bundle'),
    'help' => t('The bundle of the audited entity.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => FALSE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['audit_log']['entity_id'] = array(
    'title' => t('Entity ID'),
    'help' => t('The identifier of the audited entity.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['audit_log']['revision_id'] = array(
    'title' => t('Entity revision ID'),
    'help' => t('The revision identifier of the audited entity.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['audit_log']['action'] = array(
    'title' => t('Log action'),
    'help' => t('The action taken on the audited entity.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => FALSE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['audit_log']['timestamp'] = array(
    'title' => t('Log time'),
    'help' => t('The time at which the audited action occurred.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  $data['audit_log']['uid'] = array(
    'title' => t('User ID.'),
    'help' => t('The user ID of account that initiated the audited action.'),
    'relationship' => array(
      'base' => 'users',
      'base field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('User'),
      'title' => t('User that initiated the audited action.'),
      'help' => t('Link to user information for the user that initiated the audited action.'),
    ),
  );
  $data['audit_log']['message'] = array(
    'title' => t('Message'),
    'help' => t('Message logged during the audited action.'),
    'field' => array(
      'handler' => 'views_handler_field_markup',
      'format' => 'filtered_html',
      'click sortable' => FALSE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  return $data;
}