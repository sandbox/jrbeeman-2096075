<?php

/**
 * @file
 * Comment module callbacks.
 */

function comment_audit_log_comment_messages($comment, $action) {
  $messages = array();

  if ($action == 'update') {

    // Check published status changed.
    if ($comment->status != $comment->original->status) {
      $status = t('Unpublished');
      if ($comment->status == COMMENT_PUBLISHED) {
        $status = t('Published');
      }
      $messages[] = t('Status: @status', array('@status' => $status));
    }

    // Check author changed.
    if ($comment->uid != $comment->original->uid) {
      $new_author = user_load($comment->uid);
      $new_author_link = l($new_author->name, 'user/' . $new_author->uid);
      $messages[] = t('Author: !author', array('!author' => $new_author_link));
    }

  }

  return $messages;
}