<?php

/**
 * @file
 * Diff module callbacks for Audit log module.
 */

function diff_audit_log_node_messages($node, $action) {
  $messages = array();

  if ($action == 'update') {

    // If new revision, link to diff.
    if ($node->vid != $node->original->vid) {
      $url = 'node/' . $node->nid . '/revisions/view/' . $node->original->vid . '/' . $node->vid;
      $messages[] = l(t('View diff'), $url);
    }

  }

  return $messages;
}