<?php

/**
 * @file
 * Forum module callbacks.
 */

function forum_audit_log_node_messages($node, $action) {
  $messages = array();

  if ($action == 'update') {

    if (_forum_node_check_node_type($node)) {
      // Check forum moved.
      $wrapper = entity_metadata_wrapper('node', $node);
      $forum_new = $wrapper->taxonomy_forums->value();
      $wrapper = entity_metadata_wrapper('node', $node->original);
      $forum_original = $wrapper->taxonomy_forums->value();
      $forum_link = l($forum_new->name, 'forum/' . $forum_new->tid);
      if ($forum_new->tid != $forum_original->tid) {
        $messages[] = t('Forum: !forum', array('!forum' => $forum_link));
      }
    }

  }

  return $messages;
}