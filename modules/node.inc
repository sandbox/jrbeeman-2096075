<?php

/**
 * @file
 * Node module callbacks.
 */

function node_audit_log_node_messages($node, $action) {
  $messages = array();

  if ($action == 'update') {

    // Check publish status change.
    if ($node->status != $node->original->status) {
      $status = t('Unpublished');
      if ($node->status == NODE_PUBLISHED) {
        $status = t('Published');
      }
      $messages[] = t('Status: @status', array('@status' => $status));
    }

    // Check sticky change.
    if ($node->sticky != $node->original->sticky) {
      $status = t('No');
      if ($node->sticky == NODE_STICKY) {
        $status = t('Yes');
      }
      $messages[] = t('Sticky: @status', array('@status' => $status));
    }

    // Check promoted change.
    if ($node->promote != $node->original->promote) {
      $status = t('No');
      if ($node->promote == NODE_PROMOTED) {
        $status = t('Yes');
      }
      $messages[] = t('Promoted: @status', array('@status' => $status));
    }

    // Check author change.
    if ($node->uid != $node->original->uid) {
      $new_author = user_load($node->uid);
      $new_author_link = l($new_author->name, 'user/' . $new_author->uid);
      $messages[] = t('Author: !author', array('!author' => $new_author_link));
    }

  }

  return $messages;
}