<?php

/**
 * @file
 * User module callbacks.
 */

function user_audit_log_user_messages($account, $action) {
  $messages = array();

  if ($action == 'update') {

    // Check roles.
    $changed = FALSE;
    $original_roles = $account->original->roles;
    $new_roles = $account->roles;
    foreach ($new_roles as $rid => $value) {
      if (empty($original_roles[$rid])) {
        $changed = TRUE;
      }
    }
    foreach ($original_roles as $rid => $value) {
      if (empty($new_roles[$rid])) {
        $changed = TRUE;
      }
    }
    if ($changed) {
      $roles = user_roles(TRUE);
      $new_role_names = array();
      foreach ($account->roles as $rid => $value) {
        $new_role_names[$rid] = $roles[$rid];
      }
      $roles_string = implode(', ', $new_role_names);
      $messages[] = t('Roles: @roles', array('@roles' => $roles_string));
    }

    // Check status.
    $original_status = $account->original->status;
    $new_status = $account->status;
    if ($original_status != $new_status) {
      $status = t('Blocked');
      if ($status == 1) {
        $status = t('Active');
      }
      $messages[] = t('Status: @status', array('@status' => $status));
    }

  }

  return $messages;
}